#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <cctype>
#include "strFunction.h"

/*
 * Simple C++ Test Suite
 */

void simple_search() {
    std::cout << "Function : search_sbstr - test_simple_search" << std::endl;
    if (search_sbstr("Good morning programmer", "programmer") != 13){
        std::cout << "%TEST_FAILED% time=0 testname=simple_search (Function : search_sbstr) message=\"Good morning programmer\" -> programmer must return 13" << std::endl;
    }
}
void double_search() {
    std::cout << "Function : search_sbstr - test_double_search" << std::endl;
    if (search_sbstr("Good morning programmer, good programmer", "programmer") != 13){
        std::cout << "%TEST_FAILED% time=0 testname=double_search (Function : search_sbstr) message=\"Good morning programmer, good programmer\" -> programmer must return 13" << std::endl;
    }
}
void upper_lower_search() {
    std::cout << "Function : search_sbstr - test_upper_lower_search" << std::endl;
    if (search_sbstr("Good morning PrOGRAmmEr", "programmer") != 13){
        std::cout << "%TEST_FAILED% time=0 testname=upper_lower_search (Function : search_sbstr) message=\"Good morning PrOGRAmmEr\" -> programmer must return 13" << std::endl;
    }
}
void void_search() {
    std::cout << "Function : search_sbstr - test_void_search" << std::endl;
    if (search_sbstr("Good morning programmer", "") != -1){
        std::cout << "%TEST_FAILED% time=0 testname=void_search (Function : search_sbstr) message=\"Good morning programmer\" -> "" must return -1" << std::endl;
    }
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% search" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    std::cout << "%TEST_STARTED% simple_search (search_sbstr)" << std::endl;
    simple_search();
    std::cout << "%TEST_FINISHED% time=0  simple_search (search_sbstr)" << std::endl;

    std::cout << "%TEST_STARTED% double_search (search_sbstr)\n" << std::endl;
    double_search();
    std::cout << "%TEST_FINISHED% time=0  double_search (search_sbstr)" << std::endl;

    std::cout << "%TEST_STARTED% upper_lower_search (search_sbstr)" << std::endl;
    upper_lower_search();
    std::cout << "%TEST_FINISHED% time=0 upper_lower_search (search_sbstr)" << std::endl;

    std::cout << "%TEST_STARTED% void_search (search_sbstr)\n" << std::endl;
    void_search();
    std::cout << "%TEST_FINISHED% time=0 void_search (search_sbstr)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

