
#ifndef STRFUNCTION_H
#define STRFUNCTION_H


/*
Function    : word_count
Description : Function to count the number of words in a string.

Parameters :
 * -src : String for which we must count the number of words

Return :
 * The number of words.
 */
int word_count(const char* src);


/*
Function : search
Description : Function to get the position of a substring in a string.

Parameters :
 * -src (char*) : The string in which the substring is searched.
 * -target (char*) : The substring we want to search

Return :
 * An integer representing the index of the first character
 * of the substring inside the string. If the substring
 * is not found in the source, it returns -1.
*/
int search_sbstr(const char* src, const char* target);


/*
Function : replace_sbstr
Description : Function seeking a substring in a string.
 * If the substring exists, the function replaces the 
 * substring in the string with another substring. The 
 * new string is returned. If the substring is not present
 * a copy of the searched substring is returned.

Parameters
 * - modified_str (char*): Pointer to the modified string
 * - original_str (char*) : The original string
 * - str_to_replace (char*) : The substring that we want to modify
 * - replacement_substr ( char*): Substring that replaces the searched 
 * substring, if present.

Return : 
 * -modified_str (char*): Pointer to the modified string.If no modification
 * it returns the original string.
*/
char *replace_sbstr(char* modified_str, const char* original_str, const char* str_to_replace, const char* replacement_substr);

#endif /* STRFUNCTION_H */

