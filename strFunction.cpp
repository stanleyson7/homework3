#include <iostream>
#include <cstring>
#include <cctype>
#include "strFunction.h"

using namespace std;

int word_count(const char* src) {
    bool valide = false;
    int count = 0;
    while (*src != '\0') {
        if (*src == ' ' || *src == '\n' || *src == '\t') {
            valide = false;
        } else if (valide == false) {
            valide = true;
            ++count;
        }
        ++src;
    }
    return count;
}

int search_sbstr(const char* src, const char* target) {
    bool target_found = false;
    int target_index = -1;
    int targetToFind = 0;
    int targetCharCount;
    while (target_found == false && targetToFind <= strlen(src) && strlen(target) > 0) {
        targetCharCount = 0;
        while (tolower(src[targetToFind + targetCharCount]) == tolower(target[targetCharCount]) || targetCharCount <= strlen(target)) {
            targetCharCount++;
        }
        if (targetCharCount >= strlen(target)) {
            target_index = targetToFind;
            target_found = true;
        }
        targetToFind++;
    }
    return target_index;
}

char *replace_sbstr(char* modified_str, const char* original_str, const char* str_to_replace, const char* replacement_substr){
    int ind_cible = search_sbstr(original_str, str_to_replace);
    if (ind_cible != -1){
        for (int i=0; i<ind_cible; i++ ){
            modified_str[i] = original_str[i];
        }
        modified_str[ind_cible]='\0';
        strcat(modified_str,replacement_substr);

        strcat(modified_str,original_str+ind_cible+strlen(str_to_replace));
    }else{
        strcpy(modified_str,original_str);
    }
    return modified_str;
}
