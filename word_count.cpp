#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <cctype>
#include "strFunction.h"

/*
 * Simple C++ Test Suite
 */

void simple_count() {
    std::cout << "Function : word_count - test_simple_count" << std::endl;
    if (word_count("hello world!") != 2){
	std::cout << "%TEST_FAILED% time=0 testname=simple_count (Function : word_count) message=\"hello world!\" must return 2" << std::endl;
    }
}

void void_count() {
    std::cout << "Function : word_count - test_void_count" << std::endl;
    if (word_count("") != 0){
        std::cout << "%TEST_FAILED% time=0 testname=void_count (Function : word_count) message=\"\" must return 0" << std::endl;
    }
}
void on_space_count() {
    std::cout << "Function : word_count - test_on_space_count" << std::endl;
    if (word_count(" ") != 0){
        std::cout << "%TEST_FAILED% time=0 testname=on_space_count (Function : word_count) message=\" \" must return 0" << std::endl;
    }
}
void space_before_after() {
    std::cout << "Function : word_count - test_space_before_after" << std::endl;
    if (word_count(" Good morning programmer ") != 3){
        std::cout << "%TEST_FAILED% time=0 testname=space_before_after (Function : word_count) message=\" Good morning programmer \" must return 3" << std::endl;
    }
}
void multiple_space() {
    std::cout << "Function : word_count - test_multiple_space" << std::endl;
    if (word_count(" I    love    c++ ") != 3){
        std::cout << "%TEST_FAILED% time=0 testname=multiple_espace (Function : word_count) message=\" I    love    c++ \" must return 3" << std::endl;
    }
}
void space_tab() {
    std::cout << "Function : word_count - test_space_tab" << std::endl;
    if (word_count("  hello   world!  ") != 2){
        std::cout << "%TEST_FAILED% time=0 testname=tabulation (Function : word_count) message=\"  hello   world!  \" must return 2" << std::endl;
    }
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% word_count" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    std::cout << "%TEST_STARTED% simple_count (word_count)" << std::endl;
    simple_count();
    std::cout << "%TEST_FINISHED% time=0 simple_count (word_count)" << std::endl;

    std::cout << "%TEST_STARTED% void_count (word_count)\n" << std::endl;
    void_count();
    std::cout << "%TEST_FINISHED% time=0 void_count (word_count)" << std::endl;
    
    std::cout << "%TEST_STARTED% on_space_count (word_count)" << std::endl;
    on_space_count();
    std::cout << "%TEST_FINISHED% time=0 on_space_count (word_count)" << std::endl;

    std::cout << "%TEST_STARTED% space_before_after (word_count)\n" << std::endl;
    space_before_after();
    std::cout << "%TEST_FINISHED% time=0 space_before_after (word_count)" << std::endl;
    
    std::cout << "%TEST_STARTED% multiple_space (word_count)" << std::endl;
    multiple_space();
    std::cout << "%TEST_FINISHED% time=0 multiple_space (word_count)" << std::endl;

    std::cout << "%TEST_STARTED% space_tab (word_count)\n" << std::endl;
    space_tab();
    std::cout << "%TEST_FINISHED% time=0 space_tab (word_count)" << std::endl;
    
    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}

