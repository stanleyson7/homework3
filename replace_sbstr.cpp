#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <cctype>
#include "strFunction.h"

/*
 * Simple C++ Test Suite
 */

void simple_replace() {
    std::cout << "Function : replace_sbstr - test_simple_replace" << std::endl;
    char modified_str[100];
    
    if (strcmp(replace_sbstr(modified_str, "Hello programmer", "Hello", "Hi"), "Hi programmer") != 0) {
        std::cout << "%TEST_FAILED% time=0 testname=simple_replace (Function : replace_sbstr) message=\"Hello programmer\" must return \"Hi programmer\""<< std::endl;
    }
}
void void_replace() {
    std::cout << "Function : replace_sbstr - test_void_replace" << std::endl;
    char modified_str[100];
    
    if (strcmp(replace_sbstr(modified_str, "Hello programmer", "Hello", ""), " programmer") != 0) {
        std::cout << "%TEST_FAILED% time=0 testname=void_replace (Function : replace_sbstr) message=\"Hello programmer\" must return \" programmer\""<< std::endl;
    }
}
void void_target_replace() {
    std::cout << "Function : replace_sbstr - test_void_target_replace" << std::endl;
    char modified_str[100];
    
    if (strcmp(replace_sbstr(modified_str, "Hello programmer", "", "Hi"), "Hello programmer") != 0) {
        std::cout << "%TEST_FAILED% time=0 testname=void_target_replace (Function : replace_sbstr) message=\"Hello programmer\" must return \"Hello programmer\""<< std::endl;
    }
}
void double_target_replace() {
    std::cout << "Function : replace_sbstr - test_double_target_replace" << std::endl;
    char modified_str[100];
    
    if (strcmp(replace_sbstr(modified_str, "Hello Hello programmer", "Hello", "Hi"), "Hi Hello programmer") != 0) {
        std::cout << "%TEST_FAILED% time=0 testname=double_target_replace (Function : replace_sbstr) message=\"Hello Hello programmer\" must return \"Hi Hello programmer\""<< std::endl;
    }
}
void upper_lower_replace() {
    std::cout << "Function : replace_sbstr - test_upper_lower_replace" << std::endl;
    char modified_str[100];
    
    if (strcmp(replace_sbstr(modified_str, "HElLo programmer", "Hello", "Hi"), "Hi programmer") != 0) {
        std::cout << "%TEST_FAILED% time=0 testname=upper_lower_replace (Function : replace_sbstr) message=\"HElLo programmer\" must return \"Hi programmer\""<< std::endl;
    }
}

int main(int argc, char** argv) {
    std::cout << "%SUITE_STARTING% replace" << std::endl;
    std::cout << "%SUITE_STARTED%" << std::endl;

    std::cout << "%TEST_STARTED% simple_replace (replace_sbstr)" << std::endl;
    simple_replace();
    std::cout << "%TEST_FINISHED% time=0 simple_replace (replace_sbstr)" << std::endl;

    std::cout << "%TEST_STARTED% void_replace (replace_sbstr)\n" << std::endl;
    void_replace();
    std::cout << "%TEST_FINISHED% time=0 void_replace (replace_sbstr)" << std::endl;
    
    std::cout << "%TEST_STARTED% void_target_replace (replace_sbstr)" << std::endl;
    void_target_replace();
    std::cout << "%TEST_FINISHED% time=0 void_target_replace (replace_sbstr)" << std::endl;

    std::cout << "%TEST_STARTED% double_target_replace (replace_sbstr)\n" << std::endl;
    double_target_replace();
    std::cout << "%TEST_FINISHED% time=0 double_target_replace (replace_sbstr)" << std::endl;
    
    std::cout << "%TEST_STARTED% upper_lower_replace (replace_sbstr)" << std::endl;
    upper_lower_replace();
    std::cout << "%TEST_FINISHED% time=0 upper_lower_replace (replace_sbstr)" << std::endl;

    std::cout << "%SUITE_FINISHED% time=0" << std::endl;

    return (EXIT_SUCCESS);
}
